Source: golang-github-avast-apkverifier
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Hans-Christoph Steiner <hans@eds.org>
Section: devel
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-golang,
               golang-any,
               golang-github-avast-apkparser-dev,
               pandoc,
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-avast-apkverifier
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-avast-apkverifier.git
Homepage: https://github.com/avast/apkverifier
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/avast/apkverifier

Package: golang-github-avast-apkverifier-dev
Architecture: all
Depends: ${misc:Depends}
Description: Android APK Signature verification Go library
 apkverifier verifies the signatures on Android APK binaries.  It can
 verify APK Signature scheme v1, v2 and v3 and passes Google apksig's
 testing suite.  It should support all algorithms and downgrade attack
 protection.
 .
 Because Android can handle even broken x509 cerficates and ZIP files,
 apkverifier is using the ZipReader from apkparser package and vendors
 crypto/x509 in internal/x509andr and github.com/fullsailor/pkcs7
 (https://github.com/fullsailor/pkcs7) in the fullsailor/pkcs7 folder.
 The last two have some changes to handle some
 not-entirely-according-to-spec certificates.
 .
 Documentation on GoDoc (https://godoc.org/github.com/avast/apkverifier)

Package: apkverifier
Architecture: any
Built-Using: ${misc:Built-Using},
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Android APK Signature verification tool
 apkverifier is a simple command line tool that verifies the
 signatures on Android APK binaries.  It can verify APK Signature
 scheme v1, v2 and v3 and passes Google apksig's testing suite.  It
 should support all algorithms and downgrade attack protection.
 .
 Because Android can handle even broken x509 cerficates and ZIP files,
 apkverifier is using the ZipReader from apkparser package and vendors
 crypto/x509 in internal/x509andr and github.com/fullsailor/pkcs7
 (https://github.com/fullsailor/pkcs7) in the fullsailor/pkcs7 folder.
 The last two have some changes to handle some
 not-entirely-according-to-spec certificates.
 .
 Documentation on GoDoc (https://godoc.org/github.com/avast/apkverifier)
